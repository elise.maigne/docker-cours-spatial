A lancer pour construire le terminal : 

```
docker build -t course_spatial .
```

Pour exécuter le container

```
docker run --rm -p 8787:8787 -e PASSWORD=myCours3 course_spatial
```


OK là ça marche en local ! 


Maintenant on va envoyer le Dockerfile sur une forge. 
Le fichier .gitlab-ci.yml fait ça tout seul, il le déploie sur forgemia.


Pour le lancer à distance (ça le télécharge la première fois) :

```
docker run --rm -p 8787:8787 -e PASSWORD=myCours3 registry.forgemia.inra.fr/elise.maigne/docker_cours_spatial:R440
```

